ikanor/mysqldump
================

This Docker image runs a periodic backup script with `cron` and `mysqldump`. Based on [schnitzler/mysqldump](https://hub.docker.com/r/schnitzler/mysqldump/).

Requires the variables

* `MYSQL_HOST`
* `MYSQL_USER`
* `MYSQL_PASSWORD`
* `MYSQL_DATABASE`

for the `mysqldump` command.

You can configure periodicity by setting one or more of the environment vars:

* `HOURLY_BACKUP`
* `DAILY_BACKUP`
* `WEEKLY_BACKUP`
* `MONTHLY_BACKUP`

to a true-y value.

## Local Backup

Local backup runs out of the box. Target dir is `/backup`. You might want to mount it into a volume.

## Dropbox Backup

In order to send your backup files to a Dropbox folder you just have to pass to the container the environment vars `DROPBOX_DIRECTORY` and `DROPBOX_ACCESS_TOKEN`. If you do not have one, you can get a Dropbox access token in the [Dropbox API Explorer](https://dropbox.github.io/dropbox-api-v2-explorer/#files_upload).

## Compose example

Here is a full `docker-compose` file with a MySQL server and a backup container.

```
version: "2"
services:
  data:
    image: busybox
    volumes:
      - /var/lib/mysql
  server:
    build: .
    ports:
      - "${PORT}:3306"
    environment:
      - MYSQL_RANDOM_ROOT_PASSWORD
      - MYSQL_DATABASE
      - MYSQL_USER
      - MYSQL_PASSWORD
    volumes_from:
      - data
    restart: always
  backups:
    image: ikanor/scheduled-mysqldump:latest
    volumes:
      - /backup
    environment:
      - MYSQL_HOST=server
      - MYSQL_DATABASE
      - MYSQL_USER
      - MYSQL_PASSWORD
      - HOURLY_BACKUP
      - DAILY_BACKUP
      - WEEKLY_BACKUP
      - MONTHLY_BACKUP
      - DROPBOX_DIRECTORY
      - DROPBOX_ACCESS_TOKEN
    links:
      - server
    restart: always
```
