FROM alpine

RUN apk add --no-cache curl mysql-client

COPY backup /bin/backup
RUN chown 0:0 /bin/backup && chmod 700 /bin/backup
COPY crontab /var/spool/cron/crontabs/root
RUN chown 0:0 /var/spool/cron/crontabs/root && chmod 600 /var/spool/cron/crontabs/root

ENTRYPOINT ["crond", "-f", "-l", "8", "-d", "8"]
